// Libraries
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

// Constant
const { STATUS_PENDING, STATUS_SUCCESS } = require("../constants/user");

// Schema
const userSchema = new Schema(
  {
    fullName: String,
    email: {
      type: String,
    },
    password: String,
    avatar: String,
    status: {
      type: String,
      default: STATUS_SUCCESS,
    },
    confirmationCode: {
      type: String,
      default: ~~(Math.random() * 1000000),
    },
    messages: {
      type: Array,
      default: {},
    },
    friends: [{ type: Schema.Types.ObjectId, ref: "User" }],
  },
  { timestamps: true }
);

userSchema.methods.comparePassword = function (pass) {
  return bcrypt.compareSync(pass, this.password);
};

userSchema.index({ fullName: "text", email: "text" });

module.exports = mongoose.model("User", userSchema);
