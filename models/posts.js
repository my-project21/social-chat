const mongoose = require("mongoose");

const postSchema = new mongoose.Schema({
  title: String,
  category: String,
  content: String,
  image: String,
  created: {
    type: String,
    default: Date.now,
  },
});

module.exports = mongoose.model("Post", postSchema);
