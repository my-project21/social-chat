// Libraries
const router = require("express").Router();
const multer = require("multer");

// Multer middleware
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
  },
});

let upload = multer({
  storage: storage,
}).single("image");

// Controller
const POST = require("../controllers/post");

router.get("/", POST.fetchAllPost);
router.get("/:id", POST.fetchPostByID);
router.post("/", upload, POST.createPost);
router.patch("/:id", upload, POST.updatePost);
router.delete("/:id", POST.deletePost);

module.exports = router;
