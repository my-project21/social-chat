// Libraries
const router = require("express").Router();

// Controller
const { getMessages } = require("../controllers/message");

// Helpers
const { authenticateToken } = require("../helpers/auth");

router.post("/", getMessages);

module.exports = router;
