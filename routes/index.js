// Libraries
const express = require("express");
const router = express.Router();

// Routes
const postRoute = require("./post");
const userRoute = require("./user");
const messageRoute = require("./message");

module.exports = (app) => {
	router.use("/post", postRoute);
	router.use("/user", userRoute);
	router.use("/message", messageRoute);

	app.use("/api", router);
};
