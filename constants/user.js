// User status
const STATUS_PENDING = "pending";
const STATUS_PENDING_LABEL = "Pending";

const STATUS_SUCCESS = "success";
const STATUS_SUCCESS_LABEL = "Success";

// Message fail
const WRONG_PASS_MESS = "Password incorrect";
const USER_NOT_FOUND_MESS = "Email is not found";
const EMAIL_EXITED_MESS = "Email is exited, please try another!";
const USER_NOT_VERIFY_MESS = "Pending Account. Please Verify Your Email";
const UNAUTHORIZED_MESS = "Unauthorized token";
const INVALID_TOKEN_MESS = "Invalid token";
const PASS_NOT_STRONG_MESS = "Your password is not strong enough.";
const EMAIL_NOT_EXITED_MESS = "Email is not exited!";

// Message success
const REGISTER_SUCCESS_MESS = "Register user success!";

// Get field
const USER_FIELD = "avatar createdAt email friends fullName status";

module.exports = {
  STATUS_PENDING,
  STATUS_PENDING_LABEL,
  STATUS_SUCCESS,
  STATUS_SUCCESS_LABEL,
  WRONG_PASS_MESS,
  USER_NOT_FOUND_MESS,
  USER_NOT_VERIFY_MESS,
  UNAUTHORIZED_MESS,
  INVALID_TOKEN_MESS,
  PASS_NOT_STRONG_MESS,
  EMAIL_NOT_EXITED_MESS,
  REGISTER_SUCCESS_MESS,
  EMAIL_EXITED_MESS,
  USER_FIELD,
};
