// Libraries
const fs = require("fs");

// Models
const Post = require("../models/posts");

module.exports = class POST {
  // Fetch all posts
  static async fetchAllPost(req, res) {
    try {
      const posts = await Post.find();

      res.status(200).json(posts);
    } catch (error) {
      res.status(404).json({ message: error.message });
    }
  }
  // Fetch post by id
  static async fetchPostByID(req, res) {
    try {
      const { id = "" } = req.params;

      const post = await Post.findById(id);

      res.status(200).json(post);
    } catch (error) {
      res.status(404).json({ message: error.message });
    }
  }
  // Create post
  static async createPost(req, res) {
    try {
      const post = req.body;
      const imagename = req.file.filename;

      post.image = imagename;

      await Post.create(post);

      res.status(201).json({ message: "Post created successfully!" });
    } catch (error) {
      res.status(404).json({ message: error.message });
    }
  }
  // Update a post
  static async updatePost(req, res) {
    try {
      const { id } = req.params;
      let new_image = "";

      if (req.file) {
        new_image = req.file.filename;

        fs.unlinkSync("./uploads/" + req.body.old_image);
      } else {
        new_image = req.body.old_image;
      }
      const newPost = req.body;
      newPost.image = new_image;

      await Post.findByIdAndUpdate(id, newPost);
      res.status(200).json({ message: "Post updated successfully!" });
    } catch (error) {
      res.status(404).json({ message: error.message });
    }
  }
  // Delete a post
  static async deletePost(req, res) {
    const { id } = req.params;

    try {
      const result = await Post.findByIdAndDelete(id);

      if (result.image !== "") {
        fs.unlinkSync("./uploads/" + result.image);
      }

      res.status(200).json({ message: "Post deleted successfully!" });
    } catch (error) {
      res.status(404).json({ message: error.message });
    }
  }
};
