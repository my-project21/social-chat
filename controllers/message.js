// Libraries
const fs = require("fs");

// Helpers
const { generateAccessToken } = require("../helpers/auth");

// Constants
const { USER_FIELD } = require("../constants/user");

// Models
const User = require("../models/user");

// Constant
const {
  WRONG_PASS_MESS,
  USER_NOT_FOUND_MESS,
  STATUS_PENDING,
  USER_NOT_VERIFY_MESS,
  REGISTER_SUCCESS_MESS,
  EMAIL_EXITED_MESS,
} = require("../constants/user");

/** Create new user
 *
 * @param {*} res
 * @param {*} req
 */
const createMessage = async ({ users, message }) => {
  try {
    const userSent = await User.findOne({ email: users[0] });
    const userReceived = await User.findOne({ email: users[1] });

    userSent.messages.push({ des: users[1], ...message });
    userReceived.messages.push({ des: users[0], ...message });

    await userSent.save();
    await userReceived.save();
  } catch (error) {
    console.log(
      "🚀 ~ file: message.js ~ line 44 ~ createMessage ~ error",
      error
    );
    //
  }
};

const getMessages = async (req, res) => {
  try {
    const { from, to, page = 0, limit = 10 } = req.body;

    const messages = await User.aggregate([
      { $match: { email: from } },
      { $unwind: "$messages" },
      { $match: { "messages.des": to } },
      { $sort: { "messages.createdAt": -1 } },
      { $skip: +limit * +page },
      { $limit: +limit },
    ]);

    const newMessages = messages.map((m) => m.messages);

    res.status(200).json(newMessages);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

module.exports = {
  createMessage,
  getMessages,
};
