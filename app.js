// Libraries
require("dotenv").config();
const fs = require("fs");
const https = require("https");
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const socketIo = require("socket.io");

// User controllers
const { createMessage } = require("./controllers/message");

const app = express();
const port = process.env.PORT || 5000;

const server = https.createServer(
  {
    key: fs.readFileSync("server.key"),
    cert: fs.readFileSync("server.cert"),
  },
  app
);
// socket io
const io = socketIo(server, {
  cors: true,
  origins: ["http://localhost:3000"],
});

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("uploads"));

// Database connection
mongoose
  .connect(process.env.DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("Connected to database!");
  })
  .catch((e) => {
    console.log(e);
  });

// Public
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/public/index.html");
});

// Routes prefix
require("./routes")(app);

const rooms = [];

io.on("connect", (socket) => {
  socket.on("sendMessage", ({ users, message }) => {
    let room = rooms.find((r) =>
      r.users.every((u) => users.some((user) => user === u))
    );

    if (room) {
      io.to(room.id).emit("message", message);
    }

    createMessage({ users, message });
  });

  socket.on("typing", ({ users, value }) => {
    let room = rooms.find((r) =>
      r.users.every((u) => users.some((user) => user === u))
    );

    if (room) {
      io.to(room.id).emit("type", {
        to: users[1],
        from: users[0],
        value,
      });
    }
  });

  socket.on("outRoom", ({ id, create, received }) => {
    //
  });

  socket.on("joinRoom", ({ id, users }) => {
    let room = rooms.find((r) =>
      r.users.every((u) => users.some((user) => user === u))
    );

    if (!room) {
      rooms.push({ id, users });

      return socket.join(id);
    }

    socket.join(room.id);
  });
});

//Start server
server.listen(port, () =>
  console.log(`Server running at https://localhost:${port}`)
);
